package br.com.pupposoft.course.alura.escola.usecase.aluno;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import br.com.pupposoft.course.alura.escola.domain.aluno.Aluno;
import br.com.pupposoft.course.alura.escola.domain.aluno.CPF;
import br.com.pupposoft.course.alura.escola.dto.MatricularAlunoDto;
import br.com.pupposoft.course.alura.escola.gateway.RepositorioDeAlunos;
import br.com.pupposoft.course.alura.escola.gateway.memory.RepositorioDeAlunosEmMemoria;

class MatricularUsecaseUnitTest {

	@Test
	void alunoDeveriaSerPersistido() {
		RepositorioDeAlunos repositorioAluno = new RepositorioDeAlunosEmMemoria();//PODERIA USAR O MOCKITO, por isso é acabou sendo integrado (em memoria)
		MatricularUsecase usecase = new MatricularUsecase(repositorioAluno);
		
		String nome = "any name";
		String cpf = "999.999.999-99";
		String email = "any@mail.com";
		
		MatricularAlunoDto dto = MatricularAlunoDto.builder()
				.nomeAluno(nome)
				.cpfAluno(cpf)
				.emailAluno(email)
			.build();

		usecase.executa(dto);
		
		Aluno aluno = repositorioAluno.buscarPorCpf(new CPF(cpf));
		assertEquals(nome, aluno.getNome());
		assertEquals(cpf, aluno.getCpf().getValor());
		assertEquals(email, aluno.getEmail().getEndereco());
	}

}
