package br.com.pupposoft.course.alura.escola.domain.aluno;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import br.com.pupposoft.course.alura.escola.exception.InvalidCpfException;
import br.com.pupposoft.course.alura.escola.exception.InvalidEmailException;

class CpfUnitTest {

	@Test
	void shouldSucess() {
		String value = "999.999.999-99";
		CPF cpf = new CPF(value);
		assertEquals(value, cpf.getValor());
	}
	
	@Test
	void shouldInvalidCpfExceptionNullValue() {
		assertThrows(InvalidCpfException.class, () -> new CPF(null));
	}

	@Test
	void shouldInvalidCpfExceptionNoDots() {
		assertThrows(InvalidEmailException.class, () -> new Email("99999999999"));
	}
	
}
