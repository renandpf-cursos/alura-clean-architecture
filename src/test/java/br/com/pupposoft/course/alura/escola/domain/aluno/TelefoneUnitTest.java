package br.com.pupposoft.course.alura.escola.domain.aluno;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import br.com.pupposoft.course.alura.escola.exception.InvalidTelefoneException;

class TelefoneUnitTest {

	@Test
	void shouldSucess() {
		String ddd = "27";
		String number = "12345678";

		Telefone t = new Telefone(ddd, number);

		assertEquals(ddd, t.getDdd());
		assertEquals(number, t.getNumero());
	}

	@Test
	void shouldInvalidTelefoneExceptionInvalidDDD() {
		String ddd = "2";
		String number = "12345678";
		
		assertThrows(InvalidTelefoneException.class, () -> new Telefone(ddd, number));
	}
	
	@Test
	void shouldInvalidTelefoneExceptionInvalidNumber() {
		String ddd = "2";
		String number = "123456789";
		
		assertThrows(InvalidTelefoneException.class, () -> new Telefone(ddd, number));
	}
}
