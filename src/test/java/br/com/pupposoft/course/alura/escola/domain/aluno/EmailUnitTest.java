package br.com.pupposoft.course.alura.escola.domain.aluno;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import br.com.pupposoft.course.alura.escola.exception.InvalidEmailException;

class EmailUnitTest {

	@Test
	void shouldSucess() {
		String endereco = "test@mail.com";
		Email email = new Email(endereco);
		assertEquals(endereco, email.getEndereco());
	}

	@Test
	void shouldInvalidEmailExceptionNullValue() {
		assertThrows(InvalidEmailException.class, () -> new Email(null));
	}

	@Test
	void shouldInvalidEmailExceptionNoArroba() {
		assertThrows(InvalidEmailException.class, () -> new Email("test.mail.com"));
	}

	@Test
	void shouldInvalidEmailExceptionNoDot() {
		assertThrows(InvalidEmailException.class, () -> new Email("test@mail"));
	}

}
