package br.com.pupposoft.course.alura.escola.domain.aluno;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class AlunoUnitTest {

	@Test
	void addTelefone() {
		String ddd = "27";
		String number = "12345678";
		Aluno aluno = new Aluno(null, null, null);
		
		aluno.addTelefone(ddd, number);
		
		assertEquals(1, aluno.getTelefones().size());
		
		Telefone t = aluno.getTelefones().get(0);
		assertEquals(ddd, t.getDdd());
		assertEquals(number, t.getNumero());
	}
	
}
