package br.com.pupposoft.course.alura.escola.gateway;

import java.util.List;

import br.com.pupposoft.course.alura.escola.domain.aluno.Aluno;
import br.com.pupposoft.course.alura.escola.domain.aluno.CPF;

public interface RepositorioDeAlunos {
	void matricular(Aluno aluno);
	Aluno buscarPorCpf(CPF cpf);
	List<Aluno> listarTodosMatriculados();
}
