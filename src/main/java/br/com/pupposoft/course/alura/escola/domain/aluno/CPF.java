package br.com.pupposoft.course.alura.escola.domain.aluno;

import br.com.pupposoft.course.alura.escola.exception.InvalidCpfException;
import lombok.Getter;

@Getter
public class CPF {
	//ESTA CLASSE REPRESENTA O PADRÃO VALUE OBJECT
	
	private String valor;

	public CPF(String valor) {
		
		if(valor == null || !valor.matches("\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}")) {
			throw new InvalidCpfException();
		}
		
		this.valor = valor;
	}
}
