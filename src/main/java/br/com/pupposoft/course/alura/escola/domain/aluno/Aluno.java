package br.com.pupposoft.course.alura.escola.domain.aluno;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class Aluno {
	private CPF cpf;
	private String nome;
	private Email email;
	private List<Telefone> telefones = new ArrayList<>();
	
	public Aluno(CPF cpf, String nome, Email email) {
		this.cpf = cpf;
		this.nome = nome;
		this.email = email;
	}

	public void addTelefone(String ddd, String numero) {
		telefones.add(new Telefone(ddd, numero));
	}
	
}
