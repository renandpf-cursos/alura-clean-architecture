package br.com.pupposoft.course.alura.escola.gateway.memory;

import java.util.ArrayList;
import java.util.List;

import br.com.pupposoft.course.alura.escola.domain.aluno.Aluno;
import br.com.pupposoft.course.alura.escola.domain.aluno.CPF;
import br.com.pupposoft.course.alura.escola.exception.AlunoNaoEncontradoException;
import br.com.pupposoft.course.alura.escola.gateway.RepositorioDeAlunos;

public class RepositorioDeAlunosEmMemoria implements RepositorioDeAlunos {

	private List<Aluno> alunos = new ArrayList<>();
	
	@Override
	public void matricular(Aluno aluno) {
		alunos.add(aluno);
	}

	@Override
	public Aluno buscarPorCpf(CPF cpf) {
		return alunos.stream()
				.filter(a -> a.getCpf().getValor().equals(cpf.getValor()))
				.findAny()
				.orElseThrow(AlunoNaoEncontradoException::new);
	}

	@Override
	public List<Aluno> listarTodosMatriculados() {
		return new ArrayList<>(alunos);
	}

}
