package br.com.pupposoft.course.alura.escola.domain.aluno;

import br.com.pupposoft.course.alura.escola.exception.InvalidTelefoneException;
import lombok.Getter;

@Getter
public class Telefone {
	//ESTA CLASSE REPRESENTA O PADRÃO VALUE OBJECT
	
	private String ddd;
	private String numero;
	
	public Telefone(String ddd, String numero) {
		
		if(ddd == null || !ddd.matches("\\d{2}") || numero == null || !numero.matches("\\d{8}")) {
			throw new InvalidTelefoneException();
		}
		
		this.ddd = ddd;
		this.numero = numero;
	}
	
	
}
