package br.com.pupposoft.course.alura.escola.domain.aluno;

import br.com.pupposoft.course.alura.escola.exception.InvalidEmailException;
import lombok.Getter;

@Getter
public class Email {
	//ESTA CLASSE REPRESENTA O PADRÃO VALUE OBJECT
	
	private String endereco;

	public Email(String endereco) {
		
		if(endereco == null || !endereco.matches("^[a-zA-Z0-9._]+@[a-zA-Z0-9._]+\\.[a-zA-Z]{2,}$")) {
			throw new InvalidEmailException();
		}
		
		this.endereco = endereco;
	}
	
	
}
