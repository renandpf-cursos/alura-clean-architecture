package br.com.pupposoft.course.alura.escola.gateway;

public interface Cifrador {
	String cifrar(String valor);
	boolean validar(String valorCifrado, String valor);
}
