package br.com.pupposoft.course.alura.escola.dto;

import br.com.pupposoft.course.alura.escola.domain.aluno.Aluno;
import br.com.pupposoft.course.alura.escola.domain.aluno.CPF;
import br.com.pupposoft.course.alura.escola.domain.aluno.Email;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MatricularAlunoDto {
	private String nomeAluno;
	private String cpfAluno;
	private String emailAluno;

	public Aluno getAlunoDomain() {
		return new Aluno(new CPF(cpfAluno), nomeAluno, new Email(emailAluno));
	}
	
}
