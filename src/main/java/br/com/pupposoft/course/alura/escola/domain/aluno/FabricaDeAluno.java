package br.com.pupposoft.course.alura.escola.domain.aluno;

public class FabricaDeAluno {
	//PADRÃO: Builder
	
	private Aluno aluno;

	public FabricaDeAluno comNomeCpfEmail(String nome, String cpf, String email) {
		aluno = new Aluno(new CPF(cpf), nome, new Email(email));
		return this;
	}
	
	public FabricaDeAluno comTelefone(String ddd, String numero) {
		aluno.addTelefone(ddd, numero);
		return this;
	}
	
	public Aluno criar() {
		return aluno;
	}
	
}
