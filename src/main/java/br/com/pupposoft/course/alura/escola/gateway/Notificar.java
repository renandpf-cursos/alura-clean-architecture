package br.com.pupposoft.course.alura.escola.gateway;

import br.com.pupposoft.course.alura.escola.domain.aluno.Aluno;

public interface Notificar {

	void sendMessage(Aluno aluno);
	
}
