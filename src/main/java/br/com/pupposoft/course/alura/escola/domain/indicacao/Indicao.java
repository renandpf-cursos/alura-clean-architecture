package br.com.pupposoft.course.alura.escola.domain.indicacao;

import java.time.LocalDateTime;

import br.com.pupposoft.course.alura.escola.domain.aluno.Aluno;
import lombok.Getter;

@Getter
public class Indicao {
	
	private LocalDateTime data;
	private Aluno indicado;
	private Aluno indicante;
	public Indicao(Aluno indicado, Aluno indicante) {
		this.indicado = indicado;
		this.indicante = indicante;
		this.data = LocalDateTime.now();
	}
	
}
