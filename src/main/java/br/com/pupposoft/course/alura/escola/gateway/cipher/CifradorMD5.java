package br.com.pupposoft.course.alura.escola.gateway.cipher;

import java.security.MessageDigest;

import br.com.pupposoft.course.alura.escola.exception.ErroAoCifrarException;
import br.com.pupposoft.course.alura.escola.gateway.Cifrador;

public class CifradorMD5 implements Cifrador {

	@Override
	public String cifrar(String valor) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(valor.getBytes());
			byte[] bytes = md.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new ErroAoCifrarException();
		}
		
	}

	@Override
	public boolean validar(String valorCifrado, String valor) {
		return valorCifrado.equals(cifrar(valor));
	}

}
