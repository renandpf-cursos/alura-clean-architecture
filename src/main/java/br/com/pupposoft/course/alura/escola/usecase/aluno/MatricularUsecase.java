package br.com.pupposoft.course.alura.escola.usecase.aluno;

import br.com.pupposoft.course.alura.escola.domain.aluno.Aluno;
import br.com.pupposoft.course.alura.escola.dto.MatricularAlunoDto;
import br.com.pupposoft.course.alura.escola.gateway.RepositorioDeAlunos;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MatricularUsecase {
	
	private RepositorioDeAlunos repositorioDeAlunos;

	
	//PADRAO COMMAND (uma classe com uma ação)
	public void executa(MatricularAlunoDto matricularAlunoDto) {
		Aluno novo = matricularAlunoDto.getAlunoDomain();
		repositorioDeAlunos.matricular(novo);
	}
	
}
